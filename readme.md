# Stock Availability Manager



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

Build and run the app as a standalone jar, or using maven.

If you have maven installed you can do

```
mvn spring-boot:run
```

Once the application is running, you can manually interact with the services using the swagger ui

```
http://localhost:8080/swagger-ui.html
```


## Running the tests

The unit and integration tests are run every time the application is build or run.

You can also run them with maven using

```
mvn test
```