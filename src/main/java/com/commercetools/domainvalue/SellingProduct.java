package com.commercetools.domainvalue;

public class SellingProduct {

    private String productId;

    private Integer itemsSold;

    public SellingProduct() {}

    public SellingProduct(String productId, Integer itemsSold) {
        this.productId = productId;
        this.itemsSold = itemsSold;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getItemsSold() {
        return itemsSold;
    }

    public void setItemsSold(Integer itemsSold) {
        this.itemsSold = itemsSold;
    }
}
