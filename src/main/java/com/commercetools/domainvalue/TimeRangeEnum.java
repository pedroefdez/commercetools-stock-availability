package com.commercetools.domainvalue;

public enum TimeRangeEnum {

    TODAY,
    LAST_MONTH;

}
