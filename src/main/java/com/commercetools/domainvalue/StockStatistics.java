package com.commercetools.domainvalue;

import com.commercetools.domainobject.StockDO;

import java.util.List;

public class StockStatistics {

    private List<StockDO> topAvailableProducts;

    private List<SellingProduct> sellingProducts;

    public List<StockDO> getTopAvailableProducts() {
        return topAvailableProducts;
    }

    public void setTopAvailableProducts(List<StockDO> topAvailableProducts) {
        this.topAvailableProducts = topAvailableProducts;
    }

    public List<SellingProduct> getSellingProducts() {
        return sellingProducts;
    }

    public void setSellingProducts(List<SellingProduct> sellingProducts) {
        this.sellingProducts = sellingProducts;
    }
}
