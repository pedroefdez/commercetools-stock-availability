package com.commercetools.mapper;

import com.commercetools.datatransferobjecct.StockDTO;
import com.commercetools.datatransferobjecct.StockInfoDTO;
import com.commercetools.domainobject.StockDO;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class StockMapper {

    public static StockDO makeStockDO(StockDTO stockDTO) {

        return new StockDO(
                stockDTO.getId(),
                Date.from(Instant.parse(stockDTO.getTimestamp())),
                stockDTO.getProductId(),
                stockDTO.getQuantity());
    }

    public static StockInfoDTO makeStockInfoDTO(StockDO stockDO, Instant requestedInstant) {

        String requestedTimestampString =
                LocalDateTime.ofInstant(requestedInstant, ZoneId.of(ZoneOffset.UTC.getId()))
                .toString();
        String stockTimestamp =
                LocalDateTime.ofInstant(stockDO.getTimestamp().toInstant(), ZoneId.of(ZoneOffset.UTC.getId()))
                        .toString();

        return new StockInfoDTO.StockInfoDTOBuilder()
                .withProductId(stockDO.getProductId())
                .withRequestedTimeStamp(requestedTimestampString)
                .withStock(stockDO.getId(), stockTimestamp, stockDO.getQuantity())
                .build();
    }

    public static StockDTO makeStockDTO(StockDO stockDO) {

        return StockDTO.newBuilder()
                .withId(stockDO.getId())
                .withTimestamp(stockDO.getTimestamp().toString())
                .withProductId(stockDO.getProductId())
                .withQuantity(stockDO.getQuantity())
                .build();
    }



    public static List<StockDTO> makeStockDTOList(Collection<StockDO> cars) {
        return cars.stream()
                .map(StockMapper::makeStockDTO)
                .collect(Collectors.toList());
    }


}
