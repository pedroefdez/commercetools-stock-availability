package com.commercetools.mapper;

import com.commercetools.datatransferobjecct.StatisticsDTO;
import com.commercetools.domainobject.StockDO;
import com.commercetools.domainvalue.SellingProduct;
import com.commercetools.domainvalue.TimeRangeEnum;

import java.time.ZonedDateTime;
import java.util.List;

public class StatisticsMapper {

    public static StatisticsDTO makeStatisticsDTO(TimeRangeEnum range, ZonedDateTime requestedDate,
                                                  List<StockDO> topAvailableProducts,
                                                  List<SellingProduct> topSellingProducts) {

        return StatisticsDTO.newBuilder()
                .withRange(range.toString())
                .withRequestedTimestamp(requestedDate.toString())
                .withTopAvailableProducts(StockMapper.makeStockDTOList(topAvailableProducts))
                .withTopSellingProducts(topSellingProducts)
                .build();
    }
}
