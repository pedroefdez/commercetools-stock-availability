package com.commercetools.utils;

import com.commercetools.domainobject.StockDO;
import com.commercetools.domainvalue.SellingProduct;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class StockStatisticsGenerator {

    public List<StockDO> getTopAvailableProducts(List<StockDO> stocks, int numberOfProducts) {

        Collection<StockDO> latestStockInfoById = stocks.stream()
                .collect(Collectors.toMap(StockDO::getProductId, Function.identity(), BinaryOperator.maxBy(Comparator.comparing(StockDO::getTimestamp))))
                .values();

        List<StockDO> topAvailableProducts = latestStockInfoById.stream()
                .sorted((s1, s2) -> Integer.compare(s2.getQuantity(), s1.getQuantity()))
                .collect(Collectors.toList());

        return getTopEntriesFromList(topAvailableProducts, numberOfProducts);
    }

    public List<SellingProduct> getTopSellingProducts(List<StockDO> stocks, int numberOfProducts) {

        Map<String, List<StockDO>> stocksById = stocks.stream()
                .collect(Collectors.groupingBy(StockDO::getProductId));

        List<SellingProduct> sellingProducts = new ArrayList<>();

        for (Map.Entry<String, List<StockDO>> stockById : stocksById.entrySet()) {

            String productId = stockById.getKey();
            List<StockDO> productStocks = stockById.getValue().stream()
                    .sorted(Comparator.comparing(StockDO::getTimestamp))
                    .collect(Collectors.toList());

            sellingProducts.add(calculateSellingProductFromProductStock(productStocks));
        }

        List<SellingProduct> topSellingProducts = sellingProducts.stream()
                .sorted((s1, s2) -> Integer.compare(s2.getItemsSold(), s1.getItemsSold()))
                .collect(Collectors.toList());

        return getTopEntriesFromList(topSellingProducts, numberOfProducts);
    }

    private SellingProduct calculateSellingProductFromProductStock(List<StockDO> productStocks) {

        SellingProduct sellingProduct = new SellingProduct();
        Integer itemsSold = 0;

        for (int i = 1; i < productStocks.size(); i++) {

            StockDO currentStock = productStocks.get(i);
            StockDO previousStock = productStocks.get(i - 1);
            Integer stockDifference = previousStock.getQuantity() - currentStock.getQuantity();

            if (stockDifference > 0) {
                itemsSold += stockDifference;
            }
        }

        sellingProduct.setProductId(productStocks.get(0).getProductId());
        sellingProduct.setItemsSold(itemsSold);
        return sellingProduct;
    }

    private <T> List<T> getTopEntriesFromList(List<T> list, int numberOfEntriesToBeReturned) {

        List<T> result = new ArrayList<>();
        for (int i = 0; i < numberOfEntriesToBeReturned; i++) {
            if (list.size() > i) {
                result.add(list.get(i));
            }
        }
        return result;
    }

}