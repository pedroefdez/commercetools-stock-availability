package com.commercetools.datatransferobjecct;

import com.commercetools.domainvalue.SellingProduct;

import java.util.List;

public class StatisticsDTO {

    private String requestTimestamp;

    private String range;

    private List<StockDTO> topAvailableProducts;

    private List<SellingProduct> topSellingProducts;

    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    public String getRange() {
        return range;
    }

    public List<StockDTO> getTopAvailableProducts() {
        return topAvailableProducts;
    }

    public List<SellingProduct> getTopSellingProducts() {
        return topSellingProducts;
    }

    public StatisticsDTO() {

    }

    StatisticsDTO(String requestTimestamp, String range, List<StockDTO> topAvailableProducts, List<SellingProduct> topSellingProducts) {
        this.requestTimestamp = requestTimestamp;
        this.range = range;
        this.topAvailableProducts = topAvailableProducts;
        this.topSellingProducts = topSellingProducts;
    }

    public static StatisticsDTOBuilder newBuilder() {
        return new StatisticsDTOBuilder();
    }


    public static class StatisticsDTOBuilder {

        private String requestTimestamp;

        private String range;

        private List<StockDTO> topAvailableProducts;

        private List<SellingProduct> topSellingProducts;

        public StatisticsDTOBuilder withRequestedTimestamp(String timestamp) {
            this.requestTimestamp = timestamp;
            return this;
        }

        public StatisticsDTOBuilder withRange(String range) {
            this.range = range;
            return this;
        }

        public StatisticsDTOBuilder withTopAvailableProducts(List<StockDTO> topAvailableProducts) {
            this.topAvailableProducts = topAvailableProducts;
            return this;
        }

        public StatisticsDTOBuilder withTopSellingProducts(List<SellingProduct> topSellingProducts) {
            this.topSellingProducts = topSellingProducts;
            return this;
        }

        public StatisticsDTO build() {
            return new StatisticsDTO(
                    this.requestTimestamp,
                    this.range,
                    this.topAvailableProducts,
                    this.topSellingProducts);
        }

    }
}
