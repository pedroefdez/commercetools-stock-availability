package com.commercetools.datatransferobjecct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockDTO {

    @JsonIgnore
    private Long id;

    private String timestamp;
    private String productId;
    private Integer quantity;

    public StockDTO() {}

    public StockDTO(Long id, String timestamp, String productId, Integer quantity) {
        this.id = id;
        this.timestamp = timestamp;
        this.productId = productId;
        this.quantity = quantity;
    }

    public static StockDTOBuilder newBuilder() {
        return new StockDTOBuilder();
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getProductId() {
        return productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public static class StockDTOBuilder {

        private Long id;

        private String timestamp;

        private String productId;

        private Integer quantity;

        public StockDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public StockDTOBuilder withTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public StockDTOBuilder withProductId(String productId) {
            this.productId = productId;
            return this;
        }

        public StockDTOBuilder withQuantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public StockDTO build() {

            return new StockDTO(
                    this.id,
                    this.timestamp,
                    this.productId,
                    this.quantity
            );
        }
    }
}
