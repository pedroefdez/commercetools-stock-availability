package com.commercetools.datatransferobjecct;

public class StockInfoDTO {

    private String productId;

    private String requestedTimestamp;

    private StockDTO stock;

    StockInfoDTO(String productId, String requestedTimestamp, StockDTO stock) {
        this.productId = productId;
        this.requestedTimestamp = requestedTimestamp;
        this.stock = stock;
    }

    public String getProductId() {
        return productId;
    }

    public String getRequestedTimestamp() {
        return requestedTimestamp;
    }

    public StockDTO getStock() {
        return stock;
    }

    public static class StockInfoDTOBuilder {

        private String productId;

        private String requestedTimeStamp;

        private StockDTO stock;

        public StockInfoDTOBuilder withProductId(String productId) {
            this.productId = productId;
            return this;
        }

        public StockInfoDTOBuilder withRequestedTimeStamp(String requestedTimeStamp) {
            this.requestedTimeStamp = requestedTimeStamp;
            return this;
        }

        public StockInfoDTOBuilder withStock(Long id, String timestamp, Integer quantity) {

            this.stock = new StockDTO(id, timestamp, null, quantity);
            return this;
        }

        public StockInfoDTO build() {

            return new StockInfoDTO(
                    this.productId,
                    this.requestedTimeStamp,
                    this.stock
            );
        }


    }
}
