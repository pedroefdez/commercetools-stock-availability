package com.commercetools.controller;

import com.commercetools.datatransferobjecct.StatisticsDTO;
import com.commercetools.domainvalue.StockStatistics;
import com.commercetools.domainvalue.TimeRangeEnum;
import com.commercetools.mapper.StatisticsMapper;
import com.commercetools.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.ZonedDateTime;

@RestController
@RequestMapping("/v1/statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @GetMapping
    public StatisticsDTO getStatistics(@Valid @RequestParam TimeRangeEnum time) {

        ZonedDateTime requestTimestamp = ZonedDateTime.now();

        StockStatistics statistics = statisticsService.getStockStatisticsFromTimerange(time);

        return StatisticsMapper.makeStatisticsDTO(
                time,
                requestTimestamp,
                statistics.getTopAvailableProducts(),
                statistics.getSellingProducts());
    }
}
