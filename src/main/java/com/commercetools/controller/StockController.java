package com.commercetools.controller;

import com.commercetools.datatransferobjecct.StockDTO;
import com.commercetools.datatransferobjecct.StockInfoDTO;
import com.commercetools.domainobject.StockDO;
import com.commercetools.exception.OutdatedStockException;
import com.commercetools.exception.StockNotFoundException;
import com.commercetools.mapper.StockMapper;
import com.commercetools.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;

@RestController
@RequestMapping("/v1/stock")
public class StockController {

    @Autowired
    private StockService stockService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void updateStock(@Valid @RequestBody StockDTO stockDTO) throws OutdatedStockException {

        StockDO stockDO = StockMapper.makeStockDO(stockDTO);
        stockService.updateStock(stockDO);
    }

    @GetMapping
    public StockInfoDTO getStock(@Valid @RequestParam String productId) throws StockNotFoundException {

        Instant requestedInstant = Instant.now();

        StockDO stockDO = stockService.getStock(productId);

        return StockMapper.makeStockInfoDTO(stockDO, requestedInstant);
    }

}
