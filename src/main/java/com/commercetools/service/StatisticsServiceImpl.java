package com.commercetools.service;

import com.commercetools.utils.StockStatisticsGenerator;
import com.commercetools.dataaccessobject.StockRepository;
import com.commercetools.domainvalue.StockStatistics;
import com.commercetools.domainvalue.TimeRangeEnum;
import com.commercetools.domainobject.StockDO;
import com.commercetools.domainvalue.SellingProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.List;


@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private StockStatisticsGenerator stockStatisticsGenerator;

    @Override
    public StockStatistics getStockStatisticsFromTimerange(TimeRangeEnum timeRange) {

        Date endDate = getEndDate();
        Date startDate = getStartDateFromTimeRange(timeRange);

        List<StockDO> timeRangeStocks = stockRepository.findByTimestampAfterAndTimestampBefore(startDate, endDate);

        List<StockDO> topAvailableProducts = stockStatisticsGenerator.getTopAvailableProducts(timeRangeStocks, 3);

        List<SellingProduct> topSellingProducts = stockStatisticsGenerator.getTopSellingProducts(timeRangeStocks, 3);

        StockStatistics result = new StockStatistics();
        result.setTopAvailableProducts(topAvailableProducts);
        result.setSellingProducts(topSellingProducts);

        return result;
    }

    private Date getStartDateFromTimeRange(TimeRangeEnum timeRange) {
        
        switch (timeRange) {

            case TODAY:
                return getTodayStartDate();

            case LAST_MONTH:
                return getLastMonthStartDate();
        }

        return null;
    }

    private Date getEndDate() {
        return Date.from(ZonedDateTime.now(ZoneOffset.UTC).toInstant());
    }
    
    private Date getTodayStartDate() {
        return Date.from(ZonedDateTime.now().toLocalDate().atStartOfDay(ZoneOffset.UTC).toInstant());
    }
    
    private Date getLastMonthStartDate() {
        return Date.from(ZonedDateTime.now().toLocalDate().atStartOfDay(ZoneOffset.UTC)
                .with(TemporalAdjusters.firstDayOfMonth()).toInstant());
    }
    
    
}
