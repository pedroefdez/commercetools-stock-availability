package com.commercetools.service;

import com.commercetools.domainobject.StockDO;
import com.commercetools.exception.OutdatedStockException;
import com.commercetools.exception.StockNotFoundException;

public interface StockService {

    void updateStock(StockDO stockDO) throws OutdatedStockException;

    StockDO getStock(String productId) throws StockNotFoundException;
}
