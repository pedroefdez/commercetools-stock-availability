package com.commercetools.service;

import com.commercetools.domainvalue.StockStatistics;
import com.commercetools.domainvalue.TimeRangeEnum;

public interface StatisticsService {

    StockStatistics getStockStatisticsFromTimerange(TimeRangeEnum timeRange);
}
