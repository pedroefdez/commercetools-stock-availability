package com.commercetools.service;

import com.commercetools.dataaccessobject.StockRepository;
import com.commercetools.domainobject.StockDO;
import com.commercetools.exception.OutdatedStockException;
import com.commercetools.exception.StockNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository stockRepository;

    @Override
    @Transactional
    public void updateStock(StockDO stockDO) throws OutdatedStockException {

        StockDO currentStock = stockRepository.findFirstByProductIdOrderByIdDesc(stockDO.getProductId());

        if (currentStock != null && stockIsOutdated(stockDO, currentStock)) {

            throw new OutdatedStockException();
        }

        stockRepository.save(stockDO);
    }

    @Override
    public StockDO getStock(String productId) throws StockNotFoundException {

        StockDO stockDO = stockRepository.findFirstByProductIdOrderByIdDesc(productId);

        if (stockDO == null) {
            throw new StockNotFoundException();
        }

        return stockDO;
    }

    private boolean stockIsOutdated(StockDO stockDO, StockDO currentStock) {
        return currentStock.getTimestamp().after(stockDO.getTimestamp());
    }
}
