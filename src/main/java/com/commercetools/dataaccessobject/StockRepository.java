package com.commercetools.dataaccessobject;

import com.commercetools.domainobject.StockDO;
import org.springframework.data.repository.CrudRepository;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;


public interface StockRepository extends CrudRepository<StockDO, Long> {

    StockDO findFirstByProductIdOrderByIdDesc(String productId);

    List<StockDO> findByTimestampAfterAndTimestampBefore(Date start, Date end);
}
