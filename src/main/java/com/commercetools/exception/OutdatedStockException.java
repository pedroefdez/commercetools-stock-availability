package com.commercetools.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT, reason = "A newer stock has already been processed")
public class OutdatedStockException extends Exception {

}
