------------------------------
-- STOCK FROM TWO DAYS AGO --
------------------------------
insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-2, now()),
    'id-1',
    100
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-2, now()),
    'id-2',
    200
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-2, now()),
    'id-3',
    300
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-2, now()),
    'id-4',
    400
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-2, now()),
    'id-5',
    500
);

----------------------------
-- STOCK FROM ONE DAY AGO --
----------------------------
insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-1, now()),
    'id-1',
    99
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-1, now()),
    'id-2',
    198
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-1, now()),
    'id-3',
    297
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-1, now()),
    'id-4',
    396
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('DAY',-1, now()),
    'id-5',
    495
);

------------------------------
-- STOCK FROM TWO HOURS AGO --
------------------------------
insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-2, now()),
    'id-1',
    10
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-2, now()),
    'id-2',
    20
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-2, now()),
    'id-3',
    30
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-2, now()),
    'id-4',
    40
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-2, now()),
    'id-5',
    50
);

------------------------------
-- STOCK FROM ONE HOUR AGO --
------------------------------
insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-1, now()),
    'id-1',
    1000
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-1, now()),
    'id-2',
    200
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-1, now()),
    'id-3',
    0
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-1, now()),
    'id-4',
    0
);

insert into stock (timestamp, product_id, quantity) values (
    DATEADD('HOUR',-1, now()),
    'id-5',
    1
);