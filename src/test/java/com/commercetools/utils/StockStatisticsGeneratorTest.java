package com.commercetools.utils;

import com.commercetools.StockListsBuilder;
import com.commercetools.domainobject.StockDO;
import com.commercetools.domainvalue.SellingProduct;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@RunWith(MockitoJUnitRunner.class)
public class StockStatisticsGeneratorTest {

    @InjectMocks
    private StockStatisticsGenerator testSubject;

    @Test
    public void test_top_three_available_products() {

        List<StockDO> result = testSubject.getTopAvailableProducts(StockListsBuilder.stockDOListExample(), 3);

        assertThat(result)
                .hasSize(3)
                .extracting("productId", "quantity")
                .containsExactly(
                        tuple("id-5", 50),
                        tuple("id-4", 40),
                        tuple("id-3", 30)
                );
    }

    @Test
    public void test_top_three_when_there_are_no_stocks_then_return_empty_list() {

        List<StockDO> result = testSubject.getTopAvailableProducts(new ArrayList<>(), 3);

        assertThat(result)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void test_top_three_when_there_is_one_stock_then_return_one_element() {

        List<StockDO> result = testSubject.getTopAvailableProducts(StockListsBuilder.stockDOListOneProductExample(), 3);

        assertThat(result)
                .hasSize(1)
                .extracting("productId", "quantity")
                .containsExactly(
                        tuple("id-1", 10));
    }

    @Test
    public void test_top_three_when_there_are_tow_stocks_then_return_two_elements() {

        List<StockDO> result = testSubject.getTopAvailableProducts(StockListsBuilder.stockDOListTwoProductsExample(), 3);

        assertThat(result)
                .hasSize(2)
                .extracting("productId", "quantity")
                .containsExactly(
                        tuple("id-2", 40),
                        tuple("id-1", 10));
    }

    @Test
    public void test_top_three_selling_products() {

        List<SellingProduct> result = testSubject.getTopSellingProducts(StockListsBuilder.stockDOListExample(), 3);

        assertThat(result)
                .hasSize(3)
                .extracting("productId", "itemsSold")
                .containsExactly(
                        tuple("id-5", 450),
                        tuple("id-4", 360),
                        tuple("id-3", 270)
                );
    }


}