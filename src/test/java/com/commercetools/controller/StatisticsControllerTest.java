package com.commercetools.controller;

import com.commercetools.StockListsBuilder;
import com.commercetools.datatransferobjecct.StatisticsDTO;
import com.commercetools.domainvalue.SellingProduct;
import com.commercetools.domainvalue.StockStatistics;
import com.commercetools.domainvalue.TimeRangeEnum;
import com.commercetools.service.StatisticsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsControllerTest {

    @InjectMocks
    private StatisticsController testSubject;

    @Mock
    private StatisticsService statisticsService;

    @Test
    public void test_get_statistics_then_get_statistics() {

        when(statisticsService.getStockStatisticsFromTimerange(any(TimeRangeEnum.class))).thenReturn(getStockStatisticsExample());

        StatisticsDTO result = testSubject.getStatistics(TimeRangeEnum.TODAY);

        assertThat(result)
                .isNotNull();
    }

    @Test
    public void test_get_statistics_then_call_statistics_service() {

        when(statisticsService.getStockStatisticsFromTimerange(any(TimeRangeEnum.class))).thenReturn(getStockStatisticsExample());

        testSubject.getStatistics(TimeRangeEnum.LAST_MONTH);

        verify(statisticsService, times(1)).getStockStatisticsFromTimerange(any(TimeRangeEnum.class));
    }

    @Test
    public void test_get_statistics_with_range_then_call_statistics_service_with_same_range() {


        when(statisticsService.getStockStatisticsFromTimerange(any(TimeRangeEnum.class))).thenReturn(getStockStatisticsExample());
        TimeRangeEnum range = TimeRangeEnum.TODAY;

        testSubject.getStatistics(range);

        ArgumentCaptor<TimeRangeEnum> captor = ArgumentCaptor.forClass(TimeRangeEnum.class);
        verify(statisticsService, times(1)).getStockStatisticsFromTimerange(captor.capture());

        assertThat(captor.getValue())
                .isEqualTo(range);
    }

    @Test
    public void test_get_statistics_when_service_returns_them_then_map_them() {

        when(statisticsService.getStockStatisticsFromTimerange(any(TimeRangeEnum.class))).thenReturn(getStockStatisticsExample());

        StatisticsDTO result = testSubject.getStatistics(TimeRangeEnum.LAST_MONTH);

        assertThat(result)
                .extracting("range")
                .contains(TimeRangeEnum.LAST_MONTH.toString());

        assertThat(result.getTopSellingProducts())
                .extracting("productId", "itemsSold")
                .containsExactly(tuple("id-1", 50), tuple("id-2",  33));

        assertThat(result.getTopAvailableProducts())
                .extracting("id", "productId", "quantity")
                .containsExactly(
                        tuple(1L, "id-1", 10),
                        tuple(2L, "id-2", 20));

    }

    private StockStatistics getStockStatisticsExample() {

        StockStatistics result = new StockStatistics();
        result.setSellingProducts(Arrays.asList(new SellingProduct("id-1", 50), new SellingProduct("id-2", 33)));
        result.setTopAvailableProducts(StockListsBuilder.shortStockDOExample());

        return result;
    }
}