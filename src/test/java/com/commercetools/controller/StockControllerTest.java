package com.commercetools.controller;

import com.commercetools.datatransferobjecct.StockDTO;
import com.commercetools.datatransferobjecct.StockInfoDTO;
import com.commercetools.domainobject.StockDO;
import com.commercetools.exception.OutdatedStockException;
import com.commercetools.exception.StockNotFoundException;
import com.commercetools.service.StockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;


@RunWith(MockitoJUnitRunner.class)
public class StockControllerTest {

    @InjectMocks
    private StockController testSubject;

    @Mock
    private StockService stockService;

    @Test
    public void post_stock_then_call_service() throws Exception {

        StockDTO request = StockDTO.newBuilder()
                .withId(123L)
                .withProductId("productId")
                .withTimestamp("2017-07-16T22:54:01.754Z")
                .withQuantity(33)
                .build();

        testSubject.updateStock(request);

        verify(stockService, times(1)).updateStock(any(StockDO.class));

    }

    @Test(expected = DateTimeException.class)
    public void post_stock_with_invalid_timestamp_then_fail() throws Exception {

        StockDTO request = StockDTO.newBuilder()
                .withId(123L)
                .withProductId("productId")
                .withTimestamp("33")
                .withQuantity(33)
                .build();

        testSubject.updateStock(request);

        verify(stockService, never()).updateStock(any(StockDO.class));
    }

    @Test(expected = OutdatedStockException.class)
    public void post_outdated_stock_then_fail() throws Exception {

        StockDTO request = StockDTO.newBuilder()
                .withId(123L)
                .withProductId("productId")
                .withTimestamp("1990-04-07T22:54:01.754Z")
                .withQuantity(33)
                .build();

        doThrow(OutdatedStockException.class).when(stockService).updateStock(any(StockDO.class));

        testSubject.updateStock(request);

    }

    @Test
    public void get_stock_then_return_stock_info() throws StockNotFoundException {

        StockDO stockDO = getCompleteStockDO();

        when(stockService.getStock(anyString())).thenReturn(stockDO);

        StockInfoDTO result = testSubject.getStock("productId");

        assertThat(result)
                .isNotNull()
                .isInstanceOf(StockInfoDTO.class);
    }

    @Test
    public void get_stock_then_call_get_stock_info_service() throws StockNotFoundException {

        StockDO stockDO = getCompleteStockDO();

        when(stockService.getStock(anyString())).thenReturn(stockDO);

        StockInfoDTO result = testSubject.getStock("productId");

        verify(stockService, times(1)).getStock(anyString());
    }

    @Test(expected = StockNotFoundException.class)
    public void get_stock_when_is_not_in_db_then_fail() throws StockNotFoundException {

        doThrow(StockNotFoundException.class).when(stockService).getStock(anyString());

        StockInfoDTO result = testSubject.getStock("productId");

        verify(stockService, times(1)).getStock(anyString());
    }

    @Test
    public void get_stock_with_product_id_then_service_is_called_with_same_id() throws StockNotFoundException {

        String productId = "PRODUCT-TEST";
        StockDO stockDO = getCompleteStockDO();
        when(stockService.getStock(anyString())).thenReturn(stockDO);

        StockInfoDTO result = testSubject.getStock(productId);


        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(stockService, times(1)).getStock(captor.capture());

        String capturedProductId = captor.getValue();
        assertThat(capturedProductId)
                .isEqualTo(productId);
    }

    @Test
    public void get_stock_when_service_returns_stock_then_get_all_stock_info() throws StockNotFoundException {

        StockDO stockDO = getCompleteStockDO();

        when(stockService.getStock(anyString())).thenReturn(stockDO);

        StockInfoDTO result = testSubject.getStock("productId");

        String expectedTimestamp = LocalDateTime.ofInstant(stockDO.getTimestamp().toInstant(), ZoneId.of(ZoneOffset.UTC.getId()))
                .toString();

        assertThat(result)
                .extracting("productId", "stock.id", "stock.quantity", "stock.timestamp", "stock.productId")
                .containsOnly(stockDO.getProductId(), stockDO.getId(), stockDO.getQuantity(), expectedTimestamp, null);
    }

    private static StockDO getCompleteStockDO() {

        Long id = 1234L;
        String productId = "PRODUCT-ID";
        Integer quantity = 33;
        Date timestamp = Date.from(ZonedDateTime.now(ZoneOffset.UTC).toInstant());

        return new StockDO(id, timestamp, productId, quantity);
    }
}