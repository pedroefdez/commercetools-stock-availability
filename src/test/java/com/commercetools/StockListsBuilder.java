package com.commercetools;

import com.commercetools.domainobject.StockDO;

import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

public class StockListsBuilder {

    public static List<StockDO> stockDOListExample() {

        return Arrays.asList(
                new StockDO(1L, Date.from(Instant.now()), "id-1", 10),
                new StockDO(2L, Date.from(Instant.now()), "id-2", 20),
                new StockDO(3L, Date.from(Instant.now()), "id-3", 30),
                new StockDO(4L, Date.from(Instant.now()), "id-4", 40),
                new StockDO(5L, Date.from(Instant.now()), "id-5", 50),
                new StockDO(6L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-1", 100),
                new StockDO(7L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-2", 200),
                new StockDO(8L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-3", 300),
                new StockDO(9L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-4", 400),
                new StockDO(10L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-5", 500)
        );
    }

    public static List<StockDO> stockDOListOneProductExample() {

        return Arrays.asList(
                new StockDO(1L, Date.from(Instant.now()), "id-1", 10),
                new StockDO(2L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-1", 20),
                new StockDO(3L, Date.from(Instant.now().minus(Duration.ofDays(2))), "id-1", 30),
                new StockDO(4L, Date.from(Instant.now().minus(Duration.ofDays(3))), "id-1", 40)
        );
    }

    public static List<StockDO> stockDOListTwoProductsExample() {

        return Arrays.asList(
                new StockDO(1L, Date.from(Instant.now()), "id-1", 10),
                new StockDO(2L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-1", 20),
                new StockDO(3L, Date.from(Instant.now().minus(Duration.ofDays(2))), "id-1", 30),
                new StockDO(4L, Date.from(Instant.now().minus(Duration.ofDays(3))), "id-2", 40)
        );
    }

    public static List<StockDO> shortStockDOExample() {
        return Arrays.asList(
                new StockDO(1L, Date.from(Instant.now()), "id-1", 10),
                new StockDO(2L, Date.from(Instant.now().minus(Duration.ofDays(1))), "id-2", 20)
        );
    }
}
