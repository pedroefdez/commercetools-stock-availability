package com.commercetools.integration;

import com.commercetools.StockAvailabilityApplication;
import com.commercetools.datatransferobjecct.StockDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
    
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockAvailabilityApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockAvailabilityIntegrationTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testGetStock() {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrlWithPort("/v1/stock?productId=id-1"),
                HttpMethod.GET, entity, String.class);

        assertThat(response)
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        assertThat(response.getBody())
                .contains("\"productId\":\"id-1\"")
                .contains("\"quantity\":1000");
    }

    @Test
    public void testGetStock_when_stock_not_found_then_return_404() {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrlWithPort("/v1/stock?productId=404"),
                HttpMethod.GET, entity, String.class);

        assertThat(response)
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.NOT_FOUND);
    }

    @Test
    public void testUpdateStock() {

        StockDTO stockDTO = StockDTO.newBuilder()
                .withProductId("test-id")
                .withQuantity(300)
                .withTimestamp("2017-07-16T22:54:01.754Z")
                .build();

        HttpEntity<StockDTO> entity = new HttpEntity<StockDTO>(stockDTO, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrlWithPort("/v1/stock"),
                HttpMethod.POST, entity, String.class);

        assertThat(response)
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.CREATED);
    }

    @Test
    public void testUpdateStock_when_timestamp_is_old_then_dont_update() {

        StockDTO stockDTO = StockDTO.newBuilder()
                .withProductId("id-1")
                .withQuantity(300)
                .withTimestamp("2017-07-16T22:54:01.754Z")
                .build();

        HttpEntity<StockDTO> entity = new HttpEntity<StockDTO>(stockDTO, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrlWithPort("/v1/stock"),
                HttpMethod.POST, entity, String.class);

        assertThat(response)
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.NO_CONTENT);
    }

    @Test
    public void testGetStatistics() {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrlWithPort("/v1/statistics?time=TODAY"),
                HttpMethod.GET, entity, String.class);

        assertThat(response)
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        assertThat(response.getBody())
                .contains("\"productId\":\"id-1\"")
                .contains("\"quantity\":1000")
                .contains("\"itemsSold\":49");
    }

    private String createUrlWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
