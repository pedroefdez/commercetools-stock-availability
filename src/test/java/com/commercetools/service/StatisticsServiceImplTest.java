package com.commercetools.service;

import com.commercetools.utils.StockStatisticsGenerator;
import com.commercetools.dataaccessobject.StockRepository;
import com.commercetools.domainvalue.StockStatistics;
import com.commercetools.domainvalue.TimeRangeEnum;
import com.commercetools.domainobject.StockDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceImplTest {

    @InjectMocks
    private StatisticsServiceImpl testSubject;

    @Mock
    private StockRepository stockRepository;

    @Mock
    private StockStatisticsGenerator stockStatisticsGenerator;

    @Captor
    private ArgumentCaptor<ArrayList<StockDO>> stockDOListCaptor;

    @Test
    public void test_get_statistics_then_return_statistics() {

        StockStatistics result = testSubject.getStockStatisticsFromTimerange(TimeRangeEnum.TODAY);

        assertThat(result)
                .isNotNull()
                .isInstanceOf(StockStatistics.class);
    }

    @Test
    public void test_get_statistics_with_range_then_call_repo_for_stocks() {

        testSubject.getStockStatisticsFromTimerange(TimeRangeEnum.TODAY);

        verify(stockRepository, times(1)).findByTimestampAfterAndTimestampBefore(any(Date.class), any(Date.class));

    }

    @Test
    public void test_get_statistics_with_today_range_then_start_date_is_start_of_day() {

        testSubject.getStockStatisticsFromTimerange(TimeRangeEnum.TODAY);

        ArgumentCaptor<Date> captor = ArgumentCaptor.forClass(Date.class);

        verify(stockRepository, times(1)).findByTimestampAfterAndTimestampBefore(captor.capture(), any(Date.class));
        Date startDate = captor.getValue();

        ZonedDateTime currentTime = ZonedDateTime.now();
        ZonedDateTime startTime = currentTime.toLocalDate().atStartOfDay(ZoneOffset.UTC);
        Date expectedDate = Date.from(startTime.toInstant());

        assertThat(startDate)
                .isEqualTo(expectedDate);

    }

    @Test
    public void test_get_statistics_with_today_range_then_start_date_is_start_of_month() {

        testSubject.getStockStatisticsFromTimerange(TimeRangeEnum.LAST_MONTH);

        ArgumentCaptor<Date> captor = ArgumentCaptor.forClass(Date.class);

        verify(stockRepository, times(1)).findByTimestampAfterAndTimestampBefore(captor.capture(), any(Date.class));
        Date startDate = captor.getValue();

        ZonedDateTime currentTime = ZonedDateTime.now();
        Date expectedTime = Date.from(
                currentTime.toLocalDate().atStartOfDay(ZoneOffset.UTC)
                        .with(TemporalAdjusters.firstDayOfMonth()).toInstant());

        assertThat(startDate)
                .isEqualTo(expectedTime);

    }

    @Test
    public void test_get_statistics_calls_top_available_products() {

        List<StockDO> databaseStocks = new ArrayList<>(Arrays.asList(mock(StockDO.class)));

        when(stockRepository.findByTimestampAfterAndTimestampBefore(any(Date.class), any(Date.class))).thenReturn(databaseStocks);

        testSubject.getStockStatisticsFromTimerange(TimeRangeEnum.LAST_MONTH);

        verify(stockStatisticsGenerator, times(1)).getTopAvailableProducts(stockDOListCaptor.capture(), anyInt());

        List<StockDO> stockDOS = stockDOListCaptor.getValue();

        assertThat(stockDOS)
                .isEqualTo(databaseStocks);

    }

    @Test
    public void test_get_statistis_calls_top_selling_products() {

        List<StockDO> databaseStocks = new ArrayList<>(Arrays.asList(mock(StockDO.class)));

        when(stockRepository.findByTimestampAfterAndTimestampBefore(any(Date.class), any(Date.class))).thenReturn(databaseStocks);

        testSubject.getStockStatisticsFromTimerange(TimeRangeEnum.LAST_MONTH);

        verify(stockStatisticsGenerator, times(1)).getTopSellingProducts(stockDOListCaptor.capture(), anyInt());

        List<StockDO> stockDOS = stockDOListCaptor.getValue();

        assertThat(stockDOS)
                .isEqualTo(databaseStocks);
    }
}