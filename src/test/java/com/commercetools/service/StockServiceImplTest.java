package com.commercetools.service;

import com.commercetools.dataaccessobject.StockRepository;
import com.commercetools.domainobject.StockDO;
import com.commercetools.exception.OutdatedStockException;
import com.commercetools.exception.StockNotFoundException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StockServiceImplTest {

    @InjectMocks
    private StockServiceImpl testSubject;

    @Mock
    private StockRepository stockRepository;

    private static final String PRODUCT_ID = "productId";

    @Test
    public void when_update_stock_then_call_repository() throws OutdatedStockException {

        StockDO stockDO = new StockDO();

        testSubject.updateStock(stockDO);

        verify(stockRepository, times(1)).save(stockDO);

    }

    @Test(expected = OutdatedStockException.class)
    public void when_update_newer_stock_then_fail() throws OutdatedStockException {

        String productId = "productId";
        StockDO newStock = new StockDO();
        newStock.setProductId(productId);
        newStock.setTimestamp(Date.valueOf(LocalDate.now().minusDays(1)));

        StockDO stockInDb = new StockDO();
        stockInDb.setProductId(productId);
        stockInDb.setTimestamp(Date.valueOf(LocalDate.now()));

        when(stockRepository.findFirstByProductIdOrderByIdDesc(productId)).thenReturn(stockInDb);

        testSubject.updateStock(newStock);

        verify(stockRepository, never()).save(newStock);
    }

    @Test
    public void when_update_older_stock_then_update() throws OutdatedStockException {

        Long stockId = 1L;

        StockDO newStock = new StockDO();
        newStock.setId(stockId);
        newStock.setTimestamp(Date.valueOf(LocalDate.now().plusDays(1)));

        StockDO stockInDb = new StockDO();
        stockInDb.setId(stockId);
        stockInDb.setTimestamp(Date.valueOf(LocalDate.now()));

        when(stockRepository.findOne(stockId)).thenReturn(stockInDb);

        testSubject.updateStock(newStock);

        verify(stockRepository, times(1)).save(newStock);
    }

    @Test
    public void when_update_stock_not_in_db_then_save() throws OutdatedStockException {

        StockDO newStock = new StockDO();
        newStock.setId(1L);

        when(stockRepository.findOne(1L)).thenReturn(null);

        testSubject.updateStock(newStock);

        verify(stockRepository, times(1)).save(newStock);
    }

    @Test
    public void when_update_stock_same_timestamp_then_save() throws OutdatedStockException {

        Long stockId = 1L;
        Date timestamp = Date.valueOf(LocalDate.now());


        StockDO newStock = new StockDO();
        newStock.setId(stockId);
        newStock.setTimestamp(timestamp);

        StockDO stockInDb = new StockDO();
        stockInDb.setId(stockId);
        stockInDb.setTimestamp(timestamp);

        when(stockRepository.findOne(stockId)).thenReturn(stockInDb);

        testSubject.updateStock(newStock);

        verify(stockRepository, times(1)).save(newStock);

    }

    @Test
    public void when_get_stock_then_call_repo() throws StockNotFoundException {

        when(stockRepository.findFirstByProductIdOrderByIdDesc(PRODUCT_ID)).thenReturn(new StockDO());

        testSubject.getStock(PRODUCT_ID);

        verify(stockRepository, times(1)).findFirstByProductIdOrderByIdDesc(anyString());
    }

    @Test
    public void when_get_stock_with_product_id_then_call_repo_with_same_product() throws StockNotFoundException {

        when(stockRepository.findFirstByProductIdOrderByIdDesc(PRODUCT_ID)).thenReturn(new StockDO());

        testSubject.getStock(PRODUCT_ID);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(stockRepository, times(1)).findFirstByProductIdOrderByIdDesc(captor.capture());

        assertThat(captor.getValue())
                .isEqualTo(PRODUCT_ID);
    }

    @Test
    public void when_get_stock_in_db_return_same_stock() throws StockNotFoundException {

        StockDO dbStock = new StockDO(123L, Date.valueOf(LocalDate.now()), PRODUCT_ID, 33);

        when(stockRepository.findFirstByProductIdOrderByIdDesc(PRODUCT_ID)).thenReturn(dbStock);

        StockDO result = testSubject.getStock(PRODUCT_ID);

        verify(stockRepository, times(1)).findFirstByProductIdOrderByIdDesc(PRODUCT_ID);
        assertThat(result)
                .isEqualTo(dbStock);
    }

    @Test(expected = StockNotFoundException.class)
    public void when_get_stock_not_in_db_then_throw_exception() throws StockNotFoundException {

        when(stockRepository.findFirstByProductIdOrderByIdDesc(PRODUCT_ID)).thenReturn(null);

        testSubject.getStock(PRODUCT_ID);

        verify(stockRepository, times(1)).findFirstByProductIdOrderByIdDesc(PRODUCT_ID);
    }

}
